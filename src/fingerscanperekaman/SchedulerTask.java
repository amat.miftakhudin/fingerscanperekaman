/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fingerscanperekaman;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Date;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.TblTDokumen;
import org.json.JSONObject;
import org.json.JSONWriter;

/**
 *
 * @author Amat Miftakhudin
 */
public class SchedulerTask extends TimerTask{
    Date now;
    private FingerScanPerekaman fingerScan;
    private Verification verification;
    private String dataClipBoard;
    
    public SchedulerTask(FingerScanPerekaman fingerScan){
        this.fingerScan = fingerScan;
    }
    
    @Override
    public void run() {
        now = new Date();
        try {
            this.setDataClipBoard((String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor));
        } catch (UnsupportedFlavorException ex) {
            Logger.getLogger(SchedulerTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SchedulerTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println("Clipboard Content: " + this.getDataClipBoard());
        //System.out.println("Clipboard Content: " + this.getDataClipBoard());
        
        if(this.getDataClipBoard().contains("REKAM")){
            String clipBoardContent = this.getDataClipBoard();
            JSONObject jsonObject = new JSONObject(clipBoardContent);
            
            TblTDokumen dok = new TblTDokumen();
            dok.setDocType(Integer.parseInt(jsonObject.get("doc_type").equals("")?"1":(String) jsonObject.get("doc_type")));
            dok.setPath((String) jsonObject.get("path"));
            dok.setUserId((String) jsonObject.get("user_id"));
            dok.setClientType((String) jsonObject.get("client_type"));
            dok.setFingerId((String) jsonObject.get("finger_id"));
            
            System.out.println(clipBoardContent.toString());
            this.changeClipboardContent("TAKEN BY SCANNER APP");
            this.fingerScan.Run(dok);
            //this.fingerScan.runCapture();
        } else if (this.getDataClipBoard().contains("VERIFIKASI")){
            String clipBoardContent = this.getDataClipBoard();
            JSONObject jsonObject = new JSONObject(clipBoardContent);
            
            TblTDokumen dok = new TblTDokumen();
            dok.setDocType(Integer.parseInt(jsonObject.get("doc_type").equals("")?"1":(String) jsonObject.get("doc_type")));
            dok.setPath((String) jsonObject.get("path"));
            dok.setUserId((String) jsonObject.get("user_id"));
            dok.setClientType((String) jsonObject.get("client_type"));
            dok.setFingerId((String) jsonObject.get("finger_id"));
            
            System.out.println(clipBoardContent.toString());
            this.changeClipboardContent("TAKEN BY SCANNER APP");
            this.verification = new Verification();
            this.verification.Run(dok);
            //this.fingerScan.runCapture();
        } else if (this.getDataClipBoard().contains("SYSTEM_EXIT")){
            this.changeClipboardContent("TAKEN BY SCANNER APP");
            System.exit(0);
        }
    }

    /**
     * @return the dataClipBoard
     */
    public String getDataClipBoard() {
        return dataClipBoard;
    }

    /**
     * @param dataClipBoard the dataClipBoard to set
     */
    public void setDataClipBoard(String dataClipBoard) {
        this.dataClipBoard = dataClipBoard;
    }
    
    public void changeClipboardContent(String contents){
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(contents), new StringSelection(contents));
    }
    
}
