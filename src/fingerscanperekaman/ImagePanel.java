package fingerscanperekaman;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import com.digitalpersona.uareu.*;
import com.digitalpersona.uareu.Fid.Fiv;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class ImagePanel 
	extends JPanel
{
	private static final long serialVersionUID = 5;
	private BufferedImage m_image;
	
	public void showImage(Fid image){
		Fiv view = image.getViews()[0];
		m_image = new BufferedImage(view.getWidth(), view.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
                
		m_image.getRaster().setDataElements(0, 0, view.getWidth(), view.getHeight(), view.getImageData());
                try{
                    String currentFolder = System.getProperty("user.dir");
                    ImageIO.write( m_image, "png", new File(currentFolder+"\\image.png"));
                } catch (IOException ex) {
                    Logger.getLogger(FingerScanPerekaman.class.getName()).log(Level.SEVERE, null, ex);
                }
		repaint();
	} 
        
        public void showImage(Fid image, String newFileName){
		Fiv view = image.getViews()[0];
		m_image = new BufferedImage(view.getWidth(), view.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
                
		m_image.getRaster().setDataElements(0, 0, view.getWidth(), view.getHeight(), view.getImageData());
                try{
                    String currentFolder = System.getProperty("user.dir");
                    ImageIO.write( m_image, "png", new File("D:\\fingerscan\\image\\" + newFileName + ".png"));
                } catch (IOException ex) {
                    Logger.getLogger(FingerScanPerekaman.class.getName()).log(Level.SEVERE, null, ex);
                }
		repaint();
	} 
	
	public void paint(Graphics g) {
		g.drawImage(m_image, 0, 0, null);
	}

}
