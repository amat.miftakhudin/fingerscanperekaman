/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fingerscanperekaman;

import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.ReaderCollection;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import model.TblTDokumen;

/**
 *
 * @author saepul
 */
public class FingerScanPerekaman 
	extends JPanel
	implements ActionListener
{
	private static final long serialVersionUID = 2;
	private static final String ACT_BACK = "back";

	private JDialog       m_dlgParent;
	private CaptureThread m_capture;
	private Reader        m_reader;
	private ImagePanel    m_image;
	private boolean       m_bStreaming;
        private ReaderCollection m_collection; 
        
        private TblTDokumen tblTDok;
        private DataStorage dataStorage;
	
	private FingerScanPerekaman(){
                try{
                        m_collection = UareUGlobal.GetReaderCollection();
		} 
		catch(UareUException e) { 
			MessageBox.DpError("ReaderCollection.GetReaders()", e);
		}
                try {
                    m_collection.GetReaders();
                } catch (UareUException ex) {
                    Logger.getLogger(FingerScanPerekaman.class.getName()).log(Level.SEVERE, null, ex);
                }
                try{
                    m_reader = m_collection.get(0);
                } catch(ArrayIndexOutOfBoundsException e){
                    JOptionPane.showMessageDialog(null, "Silakan pasang perangkat (digitalPersona) terlebih dahulu. Kemudian jalankan kembali file jar ini.");
                    System.exit(0);
                }
                
		m_bStreaming = false;
		
		m_capture = new CaptureThread(m_reader, m_bStreaming, Fid.Format.ANSI_381_2004, Reader.ImageProcessing.IMG_PROC_DEFAULT);

		final int vgap = 5;
		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		setLayout(layout);

		m_image = new ImagePanel();
		Dimension dm = new Dimension(380, 380);
		m_image.setPreferredSize(dm);
		add(m_image);
		add(Box.createVerticalStrut(vgap));
		
		JButton btnBack = new JButton("OK");
		btnBack.setActionCommand(ACT_BACK);
		btnBack.addActionListener(this);
		add(btnBack);
		add(Box.createVerticalStrut(vgap));
	}
	
	private void StartCaptureThread(){
		m_capture = new CaptureThread(m_reader, m_bStreaming, Fid.Format.ANSI_381_2004, Reader.ImageProcessing.IMG_PROC_DEFAULT);
		m_capture.start(this);
	}

	private void StopCaptureThread(){
		if(null != m_capture) m_capture.cancel();
	}
	
	private void WaitForCaptureThread(){
		if(null != m_capture) m_capture.join(1000);
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getActionCommand().equals(ACT_BACK)){
			//event from "back" button
			//cancel capture
			StopCaptureThread();
		}
		else if(e.getActionCommand().equals(CaptureThread.ACT_CAPTURE)){
			//event from capture thread
			CaptureThread.CaptureEvent evt = (CaptureThread.CaptureEvent)e;
			boolean bCanceled = false;
			Fmd fmdToIdentify = null;
			Engine engine = UareUGlobal.GetEngine();
                        
			if(null != evt.capture_result){ 
                                if(null != evt.capture_result.image && Reader.CaptureQuality.GOOD == evt.capture_result.quality){
						//acquire engine
							
						
                                                        Engine.PreEnrollmentFmd prefmd = null;
							//extract features
							Fmd fmd = null;
                                                        try {
                                                            this.dataStorage = new DataStorage();
                                                            fmd = engine.CreateFmd(evt.capture_result.image, Fmd.Format.ANSI_378_2004);
                                                            String fmdStr = new String(fmd.getData());
                                                            fmdStr = Base64.getEncoder().encodeToString(fmd.getData());
                                                            
                                                            
                                                            System.out.println("fmd data: " + fmdStr );
                                                            this.tblTDok.setFmdData(fmdStr);
                                                            this.tblTDok.setFmdByte(fmd.getData());
                                                            this.dataStorage.save(this.tblTDok);
                                                            //to image
                                                            
//                                                            byte [] data = fmd.getData();
//                                                            ByteArrayInputStream bis = new ByteArrayInputStream(data);
//                                                            BufferedImage bImage2 = ImageIO.read(bis);
//                                                            ImageIO.write(bImage2, "png", new File("d:\\fingerscan\\image\\" + this.tblTDok.getUserId()+"-"+this.tblTDok.getFingerId() + ".png") );
//                                                            System.out.println("image created");
                                                            
                                                        } catch (UareUException ex) {
                                                            Logger.getLogger(FingerScanPerekaman.class.getName()).log(Level.SEVERE, null, ex);
                                                        } catch (FileNotFoundException ex) { 
                                        Logger.getLogger(FingerScanPerekaman.class.getName()).log(Level.SEVERE, null, ex);
                                    } catch (IOException ex) {
                                        Logger.getLogger(FingerScanPerekaman.class.getName()).log(Level.SEVERE, null, ex);
                                    } 
								
							//return prefmd 
							prefmd = new Engine.PreEnrollmentFmd();
							prefmd.fmd = fmd;
							prefmd.view_index = 0;
                                                        
                                                        
								
							//send success
							
						
					}
                            
				if(null != evt.capture_result.image && Reader.CaptureQuality.GOOD == evt.capture_result.quality){
					//display image
                                        String newFileName = this.tblTDok.getUserId() + "-" + this.tblTDok.getFingerId();
					m_image.showImage(evt.capture_result.image, newFileName);
                                        
                                        byte[] bt = evt.capture_result.image.getData();
                                        String strByte = "";
//                                        for(int xx=0; xx<bt.length; xx++){
//                                            strByte += bt[xx];
//                                        }
                                        byte[] aByteArray = evt.capture_result.image.getData();
                                        int width = 200;
                                        int height = 200;

                                        DataBuffer buffer = new DataBufferByte(aByteArray, aByteArray.length);

                                        //3 bytes per pixel: red, green, blue
                                        WritableRaster raster = Raster.createInterleavedRaster(buffer, width, height, 3 * width, 3, new int[] {0, 1, 2}, (Point)null);
                                        ColorModel cm = new ComponentColorModel(ColorModel.getRGBdefault().getColorSpace(), false, true, Transparency.OPAQUE, DataBuffer.TYPE_BYTE); 
                                        BufferedImage image = new BufferedImage(cm, raster, true, null);

                                   
                                        
				}
				else if(Reader.CaptureQuality.CANCELED == evt.capture_result.quality){
					//capture or streaming was canceled, just quit
					bCanceled = true;
				}
				else{
					//bad quality
					MessageBox.BadQuality(evt.capture_result.quality);
				}
			}
			else if(null != evt.exception){
				//exception during capture
				MessageBox.DpError("Capture",  evt.exception);
				bCanceled = true;
			}
			else if(null != evt.reader_status){
				MessageBox.BadStatus(evt.reader_status);
				bCanceled = true;
			}
			
			if(!bCanceled){
				if(!m_bStreaming){
					//restart capture thread
					WaitForCaptureThread();
					StartCaptureThread();
				}
			}
			else{
				//destroy dialog
				m_dlgParent.setVisible(false);
			}
		}
	}

	private void doModal(JDialog dlgParent){
		if(m_reader != null){

                    //open reader
                    try{
                                m_reader.Open(Reader.Priority.COOPERATIVE);
                    }
                    catch(UareUException e){ MessageBox.DpError("Reader.Open()", e); }

                    boolean bOk = true;
                    if(m_bStreaming){
                            //check if streaming supported
                            Reader.Capabilities rc = m_reader.GetCapabilities();
                            if(!rc.can_stream){
                                    MessageBox.Warning("Scanner ini tidak mendukung streaming.");
                                    bOk = false;
                            }
                    }

                    if(bOk){
                            //start capture thread
                            StartCaptureThread();

                            //bring up modal dialog
                            m_dlgParent = dlgParent;
                            m_dlgParent.setAlwaysOnTop(true);
                            m_dlgParent.setContentPane(this);
                            m_dlgParent.pack();
                            m_dlgParent.setLocationRelativeTo(null);
                            m_dlgParent.toFront();
                            m_dlgParent.setVisible(true);
                            m_dlgParent.dispose();

                            //cancel capture
                            StopCaptureThread();

                            //wait for capture thread to finish
                            WaitForCaptureThread();
                    }

                    //close reader
                    try{
                            m_reader.Close();
                    }
                    catch(UareUException e){ MessageBox.DpError("Reader.Close()", e); }
	
                }
        }
        
        private void doModal(JDialog dlgParent, TblTDokumen dok){
		if(m_reader != null){

                    //open reader
                    try{
                                m_reader.Open(Reader.Priority.COOPERATIVE);
                    }
                    catch(UareUException e){ MessageBox.DpError("Reader.Open()", e); }

                    boolean bOk = true;
                    if(m_bStreaming){
                            //check if streaming supported
                            Reader.Capabilities rc = m_reader.GetCapabilities();
                            if(!rc.can_stream){
                                    MessageBox.Warning("Scanner ini tidak mendukung streaming.");
                                    bOk = false;
                            }
                    }

                    if(bOk){
                            //start capture thread
                            StartCaptureThread();

                            //bring up modal dialog
                            m_dlgParent = dlgParent;
                            m_dlgParent.setAlwaysOnTop(true);
                            m_dlgParent.setContentPane(this);
                            m_dlgParent.pack();
                            m_dlgParent.setLocationRelativeTo(null);
                            m_dlgParent.toFront();
                            m_dlgParent.setVisible(true);
                            m_dlgParent.dispose();

                            //cancel capture
                            StopCaptureThread();

                            //wait for capture thread to finish
                            WaitForCaptureThread();
                    }

                    //close reader
                    try{
                            m_reader.Close();
                    }
                    catch(UareUException e){ MessageBox.DpError("Reader.Close()", e); }
	
                }
        }
        
	public void Run(){
            JDialog dlg = new JDialog((JDialog)null, "Letakkan jari Anda pada scanner.", true);
            FingerScanPerekaman capture = new FingerScanPerekaman();
            capture.doModal(dlg);
	}
        
        public void Run(TblTDokumen dok){
            this.setTblTDokumen(dok);
            JDialog dlg = new JDialog((JDialog)null, "Letakkan jari Anda pada scanner.", true);
            FingerScanPerekaman capture = new FingerScanPerekaman();
            capture.setTblTDokumen(dok);
            capture.doModal(dlg);
	}
        
        public void setTblTDokumen(TblTDokumen tblTDok){
            this.tblTDok = tblTDok;
        }
        
        public TblTDokumen getTblTDokumen(){
            return this.tblTDok;
        }
        
        public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                /*String currentFolder = System.getProperty("user.dir");
                JDialog dlg = new JDialog((JDialog)null, "Letakkan jari Anda pada scanner.", true);
                FingerScanPerekaman capture = new FingerScanPerekaman();
                capture.doModal(dlg);*/
                FingerScanPerekaman parent = new FingerScanPerekaman();
                java.util.Timer time = new java.util.Timer();
                SchedulerTask st = new SchedulerTask(parent);
                time.schedule(st, 0, 1000);
            }
        });
	}
}

