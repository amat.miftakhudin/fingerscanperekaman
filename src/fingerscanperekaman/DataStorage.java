/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fingerscanperekaman;

import com.mysql.jdbc.Driver;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.TblTDokumen;
/**
 *
 * @author Amat Miftakhudin
 */
public class DataStorage {
    
    public void save(TblTDokumen dok) throws FileNotFoundException{
    Connection conn = null;
        try (FileInputStream f = new FileInputStream("D:\\fingerscan\\db.properties")) {
            
            Properties pros = new Properties();
            pros.load(f);
            String url = pros.getProperty("db.url");
            String user = pros.getProperty("db.user");
            String password = pros.getProperty("db.password");

            // create a connection to the database
            conn = DriverManager.getConnection(url, user, password);
            // more processing here
            //JOptionPane.showMessageDialog(null, "Connected to database.", "Success", JOptionPane.INFORMATION_MESSAGE);
            System.out.println("Connected to database");
            
            String sql = "INSERT INTO tbl_t_dokumen (doc_type, path, user_id, fmd_data, client_type, finger_id, fmd_byte)"
                    + " VALUES ( "
                    + "?,?,?,?,?,?,?)";
            
            PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, dok.getDocType());
            pstmt.setString(2, dok.getPath());
            pstmt.setString(3, dok.getUserId());
            pstmt.setString(4, dok.getFmdData());
            pstmt.setString(5, dok.getClientType());
            pstmt.setString(6, dok.getFingerId());
            pstmt.setBytes(7, dok.getFmdByte());
            
            
            int rowAffected = pstmt.executeUpdate();
            if(rowAffected == 1){
                System.out.println("Inserted successfully.");
            } else {
                System.out.println("Failed to insert.");
            }
            
        } catch(SQLException e) {
           System.out.println(e.getMessage());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        } finally {
         try{
                   if(conn != null)
                     conn.close();
         }catch(SQLException ex){
                   System.out.println(ex.getMessage());
         }
        }
    }
    
    public TblTDokumen loadFmdData(TblTDokumen dok) throws FileNotFoundException{
        TblTDokumen tblTDokumen = new TblTDokumen();
        Connection conn = null;
        try (FileInputStream f = new FileInputStream("D:\\fingerscan\\db.properties")) {
            
            Properties pros = new Properties();
            pros.load(f);
            String url = pros.getProperty("db.url");
            String user = pros.getProperty("db.user");
            String password = pros.getProperty("db.password");

            // create a connection to the database
            conn = DriverManager.getConnection(url, user, password);
            // more processing here
            //JOptionPane.showMessageDialog(null, "Connected to database.", "Success", JOptionPane.INFORMATION_MESSAGE);
            System.out.println("Connected to database");
            
            String sql = "SELECT * FROM tbl_t_dokumen WHERE user_id = ? AND finger_id = ?";
            
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, dok.getUserId());
            pstmt.setString(2, dok.getFingerId());
            
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.println("User Id : " + rs.getString("user_id"));
                tblTDokumen.setUserId(rs.getString("user_id"));
                tblTDokumen.setFmdByte(rs.getBytes("fmd_byte"));
            }
        } catch(SQLException e) {
           System.out.println(e.getMessage());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        } finally {
         try{
                   if(conn != null)
                     conn.close();
         }catch(SQLException ex){
                   System.out.println(ex.getMessage());
         }
        }
        
        return tblTDokumen;
    }
    
    
//    public static void main(String args[]){
//        DataStorage ds = new DataStorage();
//        try {
//            TblTDokumen dok = new TblTDokumen();
//            dok.setDocType(1);
//            dok.setPath("D:\\fingerscan\\data");
//            dok.setUserId("123456");
//            dok.setFmdData("asldkfjklasdjflkajdfkl;ajsdfl;aksjdfl;asdjfkl;as");
//            dok.setClientType("desktop");
//            dok.setFingerId("1");
//            ds.save(dok);
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(DataStorage.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
}
