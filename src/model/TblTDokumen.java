/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Amat Miftakhudin
 */
public class TblTDokumen {
    private int docId;
    private int docType;
    private String path;
    private String userId;
    private String fmdData;
    private String clientType;
    private String fingerId;
    private byte[] fmdByte;

    /**
     * @return the docId
     */
    public int getDocId() {
        return docId;
    }

    /**
     * @param docId the docId to set
     */
    public void setDocId(int docId) {
        this.docId = docId;
    }

    /**
     * @return the docType
     */
    public int getDocType() {
        return docType;
    }

    /**
     * @param docType the docType to set
     */
    public void setDocType(int docType) {
        this.docType = docType;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the fmdData
     */
    public String getFmdData() {
        return fmdData;
    }

    /**
     * @param fmdData the fmdData to set
     */
    public void setFmdData(String fmdData) {
        this.fmdData = fmdData;
    }

    /**
     * @return the clientType
     */
    public String getClientType() {
        return clientType;
    }

    /**
     * @param clientType the clientType to set
     */
    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    /**
     * @return the fingerId
     */
    public String getFingerId() {
        return fingerId;
    }

    /**
     * @param fingerId the fingerId to set
     */
    public void setFingerId(String fingerId) {
        this.fingerId = fingerId;
    }
    
    public void setFmdByte(byte[] fmdByte){
        this.fmdByte = fmdByte;
    }
    
    public byte[] getFmdByte(){
        return this.fmdByte;
    }
    
    
    
}
